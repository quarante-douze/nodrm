---
layout: layouts/home.njk
---

Les jeux, musiques, livres, films, etc... que l'on achète sur internet sont de plus en plus verrouillés et rendent difficile de faire ce qu'on souhaite avec. Voir pire, l'accès peut nos être retiré à tout moments.

Ce verrou numérique, ce sont les DRM, et le but de cet page et de fournir des informations contre eux.

## Les DRMs

Les DRM (Digital Right Management) sont une série de verrou numérique mis sur les contenus numériques (logiciels, jeux-vidéo, musiques, film, comics, livres, audiobook, etc) pour limiter leurs utilisation dans le but d'en empêcher les copies.

Les DRM peuvent prendre plusieurs formes (surcouche forçant à être toujours en ligne, ebook chiffré devant être déchiffré par un logiciel tiers genre Adobe, etc) suivant les média et le niveau de verrouillage que souhaite l'éditeur du contenu.

Un nouveau type de verrouillage proche des DRM (qui sera compté dedans dans ce site) est le fait de ne rendre le contenu visible QUE depuis le site et/ou l'application de l'éditeur. Cela nous rend complètement dépendant du site pour les contenus que l'on a acheté.

Les DRM sont plus ou moins présent suivant les industries. Il n'est par exemple quasiment pas possible d'acheter de film sans DRM, et la plupart du temps ils ne sont carrément lisible que depuis l'application du store qui le vend. Et depuis la fermeture de Comixology, des tas de comics ne sont plus achetable qu'avec DRM, voir visiblement uniquement depuis les applications (par exemple si on achète un comics sur le site d'IDW, on ne peut pas le télécharger, que le lire depuis l'application ou le site). Pareil pour izneo, qui permet d'acheter des BD, mais lisible que depuis le site.

Cela pose soucis pour un certain nombre de raisons :

- Vous êtes limités dans avec quels appareils vous pouvez utiliser les contenus que vous acheter. Par exemple, un epub verouillé ne fonctionnera pas sur tout vos appareils, si ceux-ci ne sont pas relié à Adobe.
- L'accès peut être perdu à tout moment, comme quand Microsoft en fermant son magasin de ebook qui [à rendu des tas de livres indisponibles](https://www.wired.com/story/microsoft-ebook-apocalypse-drm/) par la coupure de son service de DRM. De nombreux jeux des années 2000 ont souffert aussi de cela, comme le montre cette vidéo de [Tech Tengeant](https://www.youtube.com/watch?v=QZYy9KzFT2w)
- Les DRMs peuvent cesser de fonctionner à cause de bug ou de soucis de vérification de la copie.
- Certains DRMs ont besoin d'une connection constante à internet pour fonctionner.
- Dans le cas des abonnements, les plateforme d'abonnement peuvent encore plus [à tout moment rendre indisponible des oeuvres](https://theconversation.com/streaming-services-are-removing-original-tv-and-films-what-this-means-for-your-favourite-show-and-our-cultural-heritage-208746) parce qu'elles n'ont pas été assez rentable, et que c'est vu comme plus rentable de simplement les retirer

Les DRMs ont donc deux effets retors principaux :
- Vous limiter dans votre capacité d'utiliser le produit que vous avez acheté
- Vous faire potentiellement perdre un jour l'accès à ce que vous avez acheté.

> Une des choses que l'on a remarqué est que les gamers ont l'habitude, un peu comme avec les DVDs, à posséder leurs jeux. C'est le changement de paradigme qui doit arriver. Ils sont devenu à l'aise avec le fait de ne plus posséder leur collection de CD et DVD. C'est une transformation un peu plus lente à arriver dans le jeu vidéo. [...] Il s'agit de se sentir à l'aise avec ne pas posséder vos jeux.
> 
> -- Philippe Trembley, propos rapporté par Christopher Dring, dans l'article "[The new Ubisoft+ and getting gamers comfortable with not owning their games](https://www.gamesindustry.biz/the-new-ubisoft-and-getting-gamers-comfortable-with-not-owning-their-games)" sur GamesIndustry.biz

[Plus d'information](https://quarante-douze.net/la-pr%C3%A9servation-vid%C3%A9oludique)

## Les campagnes

Plusieurs campagnes ont été faites contre les DRMs,

- [Defective par Design](https://www.defectivebydesign.org/) - Une campagne lancée par la FSF contre les DRM.
- [La campagne de l'EFF](https://www.eff.org/issues/drm)
- [FckDRM](https://web.archive.org/web/20200108192951/https://fckdrm.com/) - Une ancienne campagne lancée par le magasin GOG, inspiration de cette page.
- [Stop killing games](https://www.stopkillinggames.com/) - Une campagne dirigée directement aux jeux qui sont tué par l'arrêt de leur fonctionnalité online obligatoire sans patch.

## Plateformes de ventes

Il existe des moyens d'acheter des contenus sans DRMs. Voici quelques plateforme de ventes (ou d'accès à des oeuvres du domaine public) offrant accès à des contenus qui n'ont pas de verrouillages numériques.

|  Type  | Boutiques |
| --- | --- |
| Musique: | [Bandcamp](https://bandcamp.com/) [Jamendo](https://www.jamendo.com/) [MusOpen](https://musopen.org/) |
| Jeux vidéo: | [GOG.com](https://www.gog.com/) [Itch](https://itch.io/) [Gamejolt](https://gamejolt.com/) |
| Livres & BD: | [Project Gutenberg](https://www.gutenberg.org/) [Wikisource](https://wikisource.org/) [Wikilivres](https://fr.wikibooks.org/wiki/Accueil) [Comic Book+](https://comicbookplus.com/) |
| Vidéo: | [Moving Image Archive](https://archive.org/details/movies) |
| Livres audio: | [Librivox](https://librivox.org/) |

A noter que vous pouvez aussi en obtenir des manières suivantes :
- Certains humble-bundles sont DRM-free (pensez bien à vérifier que ce sont bien des DRM-free et pas des clefs pour une appli avec DRMs)
- Les MP3s achetable sur Amazon sont souvent sans DRMs (malheureusement cela aide la plateforme Amazon qui en est bourré pour des tas d'autres produits)
- Certaines maisons d'éditions comme l'Atalante font des livres sans DRMs (par exemple les pratchett sont DRM-free généralement, n'hésitez pas à regarder ça sur les sites de vente de eBooks)

## Les retirer

Une autre solution pour avoir moins de DRMs est de retirer les DRMs des oeuvres qu'on achète, ou d'utiliser des outils pour récupérer les oeuvres qu'on achète et qui ne sont disponible en ligne, pour en avoir une copie personnelle.

- [izneo-get](https://github.com/izneo-get/izneo-get) permet de récupérer en version epub/cbz les comics acheté sur Izneo, afind e ne plus avoir besoin du site ou de l'appli pour les lires.
- Il est possible de [retirer les DRM via Calibre et un plugin](https://post-tenebras-lire.net/retirer_drm_ebook_calibre/), ainsi qu'[une version pour Linux](https://mart-e.be/post/2020/05/02/enlever-le-drm-adobe-sous-linux)
- Il est possible aussi d'utiliser des logiciels pour récupérer les films sur vos DVDs et BluRay, tel que [MakeMKV](https://www.makemkv.com)
- Il existe [des guides](https://github.com/farmerbb/RED-Project) et [des outils](https://github.com/shawngmc/game-extraction-toolbox) pour extraire les jeux tournant par émulation.
- Pour les jeux sur anciennes consoles, il est possibles soit de hacker les consoles pour pouvoir récupérer les données du jeu (comme sur 3DS en suivant [3ds.guide](https://3ds.guide/)), soit en utilisant des outils dédié pour récupérer les jeux, tels que le [Retrode](https://en.wikipedia.org/wiki/Retrode).
  - Pour les jeux PS1, Saturn, MegaCD, etc. il vous suffit d'insérer votre CD dans un lecteur CD d'ordinateur et d'utiliser le logiciel [RetroArch](https://www.retroarch.com/) qui peut le lire et le récupérer.
 
Et tout cela montre toute l'absurdité de la situation. Si on veut un fichier sur lequel on a du contrôle, il est limite plus simple de pirater une oeuvre que de déverrouiller celle que l'on a acheté, toutes les autres demandent une certaine quantité de bidouillage, ou d'acheter/utiliser des logiciels.

## Crédits

Cette page est en grande partie reprise de la campagne [fckdrm](https://web.archive.org/web/20200512014223/https://fckdrm.com/french.html) lancée par le site de vente Gog il y a quelques années et abandonnée depuis. Le but est de faire un hub sur diverses ressources contre les DRMs, pour s'en préserver et/ou les contourner.

Ce site web a été généré via [Eleventy](https://www.11ty.dev/), et son favicon provient du projet d'icone Tango.