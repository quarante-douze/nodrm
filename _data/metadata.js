module.exports = {
	title: "Non aux DRMs",
	url: "https://nodrm.quarante-douze.net/",
	language: "fr",
	description: "Un petit site pour lutter contre les DRM",
	source: "https://git.kobold.cafe/quarante-douze/nodrm",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
